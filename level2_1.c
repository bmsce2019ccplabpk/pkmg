#include <stdio.h>
int input()
{
    int n;
    scanf("%d",&n);
    return n;
}
void input_elements(int n,int a[])
{
    for(int i=0;i<n;i++)
        scanf("%d",&a[i]);
}
void compute(int n,int a[],int j[])
{
   int k = a[0],l=0;
  for (int i = 1; i <n; i++)
    {
      if (a[i] < a[0])
	{
	  j[l] = a[i];
	  l++;
	}
    }
  j[l] = k;
  l++;
  for (int i = 1; i <n; i++)
    {
      if (a[i] > a[0])
	  {
	      j[l] = a[i];
	      l++;
	  }
	  }   
}
void output(int n,int j[])
{
    for(int i=0;i<n;i++)
    printf("%d ",j[i]);
    printf("\n");
}
int main()
{
    int a[100],j[100],n;
    printf("How many elements do you want to enter\n");
    n=input();
    printf("Enter the elements\n");
    input_elements(n,a);
    compute(n,a,j);
    output(n,j);
    return 0;
}