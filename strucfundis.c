#include <stdio.h>
#include <math.h>
struct point
{
    float x,y;
};
struct points
{
    struct point q;
    struct point m;
};
typedef struct point Point;
typedef struct points Points;
Point input()
{
    Point p;
    printf("Enter the absicssa\n");
    scanf("%f",&p.x);
    printf("Enter the ordinate\n");
    scanf("%f",&p.y);
    return p;
}
Points pointse()
{
    Points p;
    p.q=input();
    p.m=input();
    return p;
}
void compute(Points a)
{
    float d;
    d=sqrt(pow((a.q.x-a.m.x),2)+pow((a.q.y-a.m.y),2));
    printf("%f\n",d);
}
int main()
{
    Points a;
    a=pointse();
    compute(a);
    return 0;
}
