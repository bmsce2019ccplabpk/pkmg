#include <stdio.h>
#include <stdlib.h>
float input(float a);
float dis(float x1,float y1,float x2,float y2);
void print(float);
int main()
{
    float a,x1,x2,y1,y2,d;
    printf("x1=");
    x1=input(a);
    printf("y1=");
    y1=input(a);
    printf("x2=");
    x2=input(a);
    printf("y2=");
    y2=input(a);
    d=dis(x1,y1,x2,y2);
    print(d);
    return 0;
}
float input(float a)
{
    scanf("%f",&a);
    return a;
}
float dis(float x1,float y1,float x2,float y2)
  {
      float dis=sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
      return dis;
  }
void print(float d)
{
    printf("The distance between the two points is %f\n",d);
}
