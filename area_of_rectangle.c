#include <stdio.h>
#include <math.h>
struct point
{
    float x,y;
};
typedef struct point Point;
struct rectangle
{
    struct point a,b,c;
    float d[3];
    struct lengths
    {
        float lenght,breadth;
    }l;
    float area;
};
typedef struct rectangle Rectangle;
float in_it(Rectangle *p);
void rectangle_points(int n,Rectangle all[]);
void compute(int n,Rectangle all[],float areas[]);
void display(int n,Rectangle all[],float areas[]);
int input_n();
Point input_point();
int main()
{
    Rectangle all[100];float areas[100];
    int n;
    printf("How many rectangles are to be analaysed\n");
    n=input_n();
    rectangle_points(n,all);
    compute(n,all,areas);
    display(n,all,areas);
    return 0;
}
int input_n()
{
    int n;
    scanf("%d",&n);
    return n;
}
void rectangle_points(int n,Rectangle all[])
{
    for(int i=0;i<n;i++)
    {
        printf("Enter the points of rectangle %d (First x-coordinate and then y-coordinate)\n",i+1);
        all[i].a=input_point();
        all[i].b=input_point();
        all[i].c=input_point();
    }
}
Point input_point()
{
    Point p;
    scanf("%f%f",&p.x,&p.y);
    return p;
}
void compute(int n,Rectangle all[],float areas[])
{
    for(int i=0;i<n;i++)
        areas[i]=in_it(&all[i]);
}
float in_it(Rectangle *p)
{
    p->d[0]=sqrt(pow((p->a.x-p->b.x),2) + pow((p->a.y-p->b.y),2));
    p->d[1]=sqrt(pow((p->a.x-p->c.x),2) + pow((p->a.y-p->c.y),2));
    p->d[2]=sqrt(pow((p->b.x-p->c.x),2) + pow((p->b.y-p->c.y),2));
    if(p->d[0]>p->d[1] && p->d[0]>p->d[2])
    {
        p->l.lenght=p->d[1]>p->d[2]?p->d[1]:p->d[2];
        p->l.breadth=p->d[1]<p->d[2]?p->d[1]:p->d[2];
        p->area=p->l.lenght*p->l.breadth;
        return p->area;
    }
    else if(p->d[1]>p->d[0] && p->d[1]>p->d[2])
    {
        p->l.lenght=p->d[0]>p->d[2]?p->d[0]:p->d[2];
        p->l.breadth=p->d[0]<p->d[2]?p->d[0]:p->d[2];
        p->area=p->l.lenght*p->l.breadth;
        return p->area;
    }
    else
    {
        p->l.lenght=p->d[0]>p->d[1]?p->d[0]:p->d[1];
        p->l.breadth=p->d[0]<p->d[1]?p->d[0]:p->d[1];
        p->area=p->l.lenght*p->l.breadth;
        return p->area;
    }
}
void display(int n,Rectangle all[],float areas[])
{
    for(int i=0;i<n;i++)
    {
        printf("The area of the rectangle %d with coordinates (%f,%f),(%f,%f),(%f,%f) is %f\n",i+1,all[i].a.x,all[i].a.y,all[i].b.x,all[i].b.y,all[i].c.x,all[i].c.y,areas[i]);
    }
}
