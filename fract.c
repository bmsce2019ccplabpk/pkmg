#include <stdio.h>
struct fract
{
    int num,den;
};
typedef struct fract Fract;
void input(Fract *p,Fract *q);
Fract add(Fract,Fract);
void display(Fract);
int gcd(Fract);
int main()
{
    Fract a,b,c;
    input(&a,&b);
    c=add(a,b);
    printf("The sum of %d/%d + %d/%d = ",a.num,a.den,b.num,b.den);
    display(c);
    return 0;
}
void input(Fract *p,Fract *q)
{
    printf("Enter the first fraction\n");
    scanf("%d%d",&p->num,&p->den);
    printf("Enter the second fraction\n");
    scanf("%d%d",&q->num,&q->den);
}
Fract add(Fract a,Fract b)
{
   Fract k;
   int h;
   k.den=a.den*b.den;
   k.num=a.num*b.den + b.num*a.den;
   h=gcd(k);
   k.num=k.num/h;
   k.den=k.den/h;
   return k;
}
void display(Fract p)
{
    printf("%d/%d = %f\n",p.num,p.den,(float)p.num/p.den);
}
int gcd(Fract a)
{
   int m,h;
   m=a.num<a.den?a.num:a.den;
   for(int i=1;i<=m;i++)
   if(a.den%i==0&&a.num%i==0)
   h=i;
   return h;
}
