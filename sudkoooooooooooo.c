#include <stdio.h>
struct boards
{
    int b[9][9];
    int viability_number;
    struct remember
    {
        int row,column,num_r,num_c,value_r,value_c,value_box,box;
    }rem[9];
};
typedef struct boards Boards;
int input_();
void input_elements(int n,Boards B[]);
void viability_checker(int n,Boards B[]);
void row_checker(int a,Boards B[]);
void column_checker(int a,Boards B[]);
void box_checker(int a,Boards B[]);
int box_error(int a,int i,Boards B[]);
void display(int n,Boards B[]);
int main()
{
    int n;
    printf("How many boards dou you want to check\n");
    n=input_();
    Boards B[n];
    input_elements(n,B);
    viability_checker(n,B);
    display(n,B);
        
}
int input_()
{
    int a;
    scanf("%d",&a);
    return a;
}
void input_elements(int n,Boards B[])
{
    for(int a=0;a<n;a++)
    {
         printf("Enter the elments of board %d\n",a+1);
         for(int r=0;r<9;r++)
            for(int c=0;c<9;c++)
         scanf("%d",&B[a].b[r][c]);
    }
}
void viability_checker(int n,Boards B[])
{
    for(int a=0;a<n;a++)
    {
        B[a].viability_number=0;
        column_checker(a,B);
        row_checker(a,B);
        box_checker(a,B);
        int r_error=0; 
        for(int r=0;r<9;r++)
        {
            if(B[a].rem[r].num_r!=0 && B[a].rem[r].value_r>1)
            {
                r_error++;
            }
        }
    
        int c_error=0;
        for(int c=0;c<9;c++)
        {
            if(B[a].rem[c].num_c!=0 && B[a].rem[c].value_c>1)
            {
                c_error++;
            }
        }
        if(r_error==0 && c_error==0)
        {
           
            int y=0;
            for(int u=0;u<9;u++)
            {
                if(B[a].rem[u].box==0)    
                {
                    y=0;
                }
                else
                {
                    y++;
                }
                if(y==0)
                {
                    int l=0;
                    for(int l1=0;l1<9;l1++)
                    for(int l2=0;l2<9;l2++)
                    {
                        if(B[a].b[l1][l2]==0)
                        {
                            l++;
                        }
                    }
                 if(l==0)
                 { 
                    B[a].viability_number=1;
                 } 
                else if(l>0)
                {
                    B[a].viability_number=2;
                    
                }
                    
                }
                
            }
        }
    }

}
void row_checker(int a,Boards B[])
{
    for(int r=0;r<9;r++)
            for(int c=0;c<9;c++)
            {
                    B[a].rem[r].row=0;
                    B[a].rem[r].num_r=0;                           
                    B[a].rem[r].value_r=0;    
                    for(int j=0;j<9;j++)
                    {
                        if(B[a].b[r][j]==B[a].b[r][c] && B[a].b[r][c]!=0)
                        {
                            B[a].rem[r].row=r;
                            B[a].rem[r].num_r=B[a].b[r][j];                           
                            B[a].rem[r].value_r++;
                        }
                    }
           }
}
void column_checker(int a,Boards B[])
{
    for(int c=0;c<9;c++)
            for(int r=0;r<9;r++)
            {
                    B[a].rem[c].column=0;
                    B[a].rem[c].num_c=0;                           
                    B[a].rem[c].value_c=0;    
                    for(int j=0;j<9;j++)
                    {
                        if(B[a].b[j][c]==B[a].b[r][c] && B[a].b[r][c]!=0)
                        {
                            B[a].rem[c].column=r;
                            B[a].rem[c].num_c=B[a].b[j][c];                           
                            B[a].rem[c].value_c++;
                        }
                    }         
            }
}
void box_checker(int a,Boards B[])
{
    for(int i=0;i<9;i++)
    {
        B[a].rem[i].box=box_error(a,i,B);
        if(B[a].rem[i].box==1)
        {
            B[a].rem[i].value_box=i+1;
        }
        else
        {
            B[a].rem[i].value_box=0;
        }
    }
    
}
int box_error(int a,int i,Boards B[])
{
    int r=0;
    int reference[9];
    for(int c=0;c<3;c++)
        for(int j=0;j<3;j++)
        {   
            if(i==0)
            {
                reference[r]=B[a].b[c][j];    
            }
            else if(i==1)
            {
                reference[r]=B[a].b[c][j+3];
            }
            else if(i==2)
            {
                reference[r]=B[a].b[c][j+6];
            }
            else if(i==3)
            {
                reference[r]=B[a].b[c+3][j];
            }
            else if(i==4)
            {
                reference[r]=B[a].b[c+3][j+3];
            }
            else if(i==5)
            {
                reference[r]=B[a].b[c+3][j+6];
            }
            else if(i==6)
            {
                reference[r]=B[a].b[c+6][j];
            }
            else if(i==7)
            {
                reference[r]=B[a].b[c+6][j+3];
            }
            else 
            {
                reference[r]=B[a].b[c+6][j+6];
            }
            r++;
        }
     r=0;
     for(int q=0;q<9;q++)
     {
        for(int w=q+1;w<9;w++)
        {
            if(reference[q]==reference[w] && reference[w]!=0)
            {
                r++;
            }
        }
     }
     if(r!=0)
     {
        return 1;
     } 
     else
     {
        return 0;
     }
}
void display(int n,Boards B[])
{
    for(int i=0;i<n;i++)
    {
            if(B[i].viability_number==1)
            {
                printf("Complete\n");
            }
            else if(B[i].viability_number==2)
            {
                printf("Incomplete but viable\n");
            }
        else
        {
            printf("Incompelete\n");
            printf("ERROR rows are : ");
            for(int r=0;r<9;r++)
                {
            if(B[i].rem[r].num_r!=0 && B[i].rem[r].value_r>1)
            {
                printf("%d ",r+1);
            }
            }
            printf("\nERROR columns are: ");
            for(int c=0;c<9;c++)
            {
                if(B[i].rem[c].num_c!=0 && B[i].rem[c].value_c>1)
                {
                    printf("%d ",c+1);
                }
            }
            printf("\nSubmatrices:");
            for(int z=0;z<9;z++)
            {
                if(B[i].rem[z].value_box!=0)
                {
                    printf("%d ",B[i].rem[z].value_box);
                }
            }
        }
    }
}