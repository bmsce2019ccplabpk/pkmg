#include <stdio.h>
struct board
{
    struct box
    {
        int elements[3][3];
    }b[9];
    int all[9][9];
};
struct remember
{
    int row[9],column[9];
    int matrix,count;
};
typedef struct remember Remember;
typedef struct box Boxes;
typedef struct board Boards;
int input_n();
void input_elements(int n,Boards s[]);
void viability_checker(int n,Boards s[],Remember cell[]);
void display(int n,Boards s[],Remember cell[]);
int main()
{
    Boards s[100];
    Remember cell[100];
    int n;
    printf("How many suduko boards are to be checked\n");
    n=input_n();
    input_elements(n,s);
    viability_checker(n,s,cell);
}
int input_n()
{
    int n;
    scanf("%d",&n);
    return n;
}
void input_elements(int n,Boards s[])
{
    for(int i=0;i<n;i++)
    {
        printf("Enter the elements of Board %d\n",i+1);
        for(int j=0;j<9;j++)
        {
            printf("Enter the elements of box %d\n",j+1);
            for(int k=0;k<3;k++)
            for(int l=0;l<3;l++)
            {
                scanf("%d",&s[i].b[j].elements[k][l]);
                switch(j)
                {
                    case 0: s[i].all[k][l]=s[i].b[j].elements[k][l];
                            break;
                    case 1: s[i].all[k][l+3]=s[i].b[j].elements[k][l];
                            break;
                    case 2: s[i].all[k][l+6]=s[i].b[j].elements[k][l];
                            break;
                    case 3: s[i].all[k+3][l]=s[i].b[j].elements[k][l];
                            break;
                    case 4: s[i].all[k+3][l+3]=s[i].b[j].elements[k][l];
                            break;
                    case 5: s[i].all[k+3][l+6]=s[i].b[j].elements[k][l];
                            break;
                    case 6: s[i].all[k+6][l]=s[i].b[j].elements[k][l];
                            break;
                    case 7: s[i].all[k+6][l+3]=s[i].b[j].elements[k][l];
                            break;
                    case 8: s[i].all[k+6][l+6]=s[i].b[j].elements[k][l];
                            break;
                }

            }
        }

    }
}
void viability_checker(int n,Boards s[],Remember cell[])
{
    for(int k=0;k<n;k++)
    {
        
    /*Row checker*/
    for(int c=0;c<9;c++)
    {
        for(int r=0;r<9;r++)
        {
            for(int j=c+1;j<9;j++)
            {
                if(s[k].all[j][r]==0)
                {
                    cell[k].row[r]=0;
                    break;
                }
                else if(s[k].all[c][r]==s[k].all[j][r])
                {
                    cell[k].row[r]=r;
                }
                else
                {
                    cell[k].row[r]=0;
                }
            }

        }
    }
        /*Column checker*/
    for(int r=0;r<9;r++)
    {
        for(int c=0;c<9;c++)
        {
            for(int j=r+1;j<9;r++)
            {
                if(s[k].all[j][r]==0)
                {
                    cell[k].column[c]=0;
                    break;
                }
                else if(s[k].all[c][r]==s[k].all[c][j])
                {
                    cell[k].column[c]=c;
                }
                else
                {
                    cell[k].column[c]=0;
                }
            }

        }
    for(int box=0;box<9;box++)
    {
        for(int i=0;i<3;i++)
        for(int j=0;j<3;j++)
        {
            cell[k].count=0;
            for(int w=0;w<3;w++)
            for(int e=0;e<3;e++)
            if(s[k].b[box].elements[i][j]==0)
            break;
            else if(s[k].b[box].elements[i][j]==s[k].b[box].elements[w][e])
            {
                cell[k].count++;
            }
            if(cell[k].count>1)
            {
                cell[k].matrix=box+1;
            }
        }
    }
    }
    int value1=1,value2=1;
    for(int c=0;c<9;c++)
    {
        if(cell[k].column[c]!=0)
        {
            value1=0;
            break;
        }

    }
    for(int r=0;r<9;r++)
    {
        if(cell[k].row[r]!=0)
        {
            value2=0;
        }
    }
    if(value1==0 || value2==0)
    {
        printf("Non-Viable\n");

    }
    if(value1!=0 && value2!=0)
    printf("\nComplete");
}

}
