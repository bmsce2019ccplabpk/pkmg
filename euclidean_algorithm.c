#include <stdio.h>
int gcd(int ,int );
int main()
{
    int x,y,h;
    printf("Enter any two numbers\n");
    scanf("%d%d",&x,&y);
    h=gcd(x,y);
    printf("The gcd of the two numbers is %d",h);
    return 0;
}
int gcd(int x,int y)
{
    int t;
    while(y!=0)
    {
        t=y;
        y=x%y;
        x=t;
    }
    return x;
}