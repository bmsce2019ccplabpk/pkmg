#include <stdio.h>
void swap(int *p,int *q);
int main()
{
    int x,y,*p,*q;
    printf("Enter any two numbers\n");
    printf("x:");
    scanf("%d",&x);
    printf("y:");
    scanf("%d",&y);
    p=&x;
    q=&y;
    printf("Before swapping x=%d y=%d\n",x,y);
    swap(p,q);
    printf("After swapping x=%d y=%d\n",x,y);
    return 0;
}
void swap(int *p,int *q)
{
    int t;
    t=*p;
    *p=*q;
    *q=t;
}
