#include <stdio.h>
#include <math.h>
int main()
{
    float x1,y1,x2,y2,distance;
    printf("Enter the coordinates of two points\n");
    printf(" x1=");
    scanf("%f",&x1);
    printf("\n y1=");
    scanf("%f",&y1);
    printf("\n x2=");
    scanf("%f",&x2);
    printf("\n y2=");
    scanf("%f",&y2);
    distance=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
    printf("The distance between two points is %f\n",distance);
    return 0;
}
