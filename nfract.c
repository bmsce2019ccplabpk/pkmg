#include <stdio.h>
typedef struct fract Fract;
struct fract
{
    int num,den;
};
int input();
Fract add(int n,Fract p[]);
void inputs(int n,Fract p[]);
void display(Fract b,Fract a[],int n);
int gcd(Fract);
int main()
{
    Fract a[100],b;
    int n;
    printf("How many fractions do you want to enter\n");
    n=input();
    inputs(n,a);
    b=add(n,a);
    display(b,a,n);
    return 0;

}
int input()
{
    int a;
    scanf("%d",&a);
    return a;
}
void inputs(int n,Fract p[])
{
    for(int i=0;i<n;i++)
    {
        printf("Enter the %d Fraction\n",i+1);
        scanf("%d%d",&p[i].num,&p[i].den);
    }
}
Fract add(int n,Fract p[])
{
    Fract k;int h;
    k.den=1;
    k.num=0;
    for(int i=0;i<n;i++)
        k.den=k.den*p[i].den;
    for(int i=0;i<n;i++)
        k.num+=(p[i].num*k.den)/p[i].den;
    h=gcd(k);
    k.num=k.num/h;
    k.den=k.den/h;
    return k;
}
int gcd(Fract k)
{
    int t;
    while(k.den!=0)
    {
        t=k.den;
        k.den=k.num%k.den;
        k.num=t;
    }
    return k.num;
}
void display(Fract b,Fract a[],int n)
{
    int i=0;
    for(;i<n-1;i++)
    {

        printf("%d/%d + ",a[i].num,a[i].den);

    }
    printf("%d/%d = %d/%d = %f\n",a[i].num,a[i].den,b.num,b.den,(float)b.num/b.den);
}
