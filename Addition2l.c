#include <stdio.h>
float input();
float compute(float a,float b);
void display(float a,float b, float c);
int main()
{
    float a,b,c;
    a=input();
    b=input();
    c=compute(a,b);
    display(a,b,c);
    return 0;
}
float input()
{
    float a;
    printf("Enter any number\n");
    scanf("%f",&a);
    return a;
}
float compute(float a,float b)
{
    return a+b;
}
void display(float a,float b, float c)
{
    printf("%f + %f = %f",a,b,c);
}
