#include <stdio.h>
int input();
void input_array(int a[],int k);
void compute(int a[],int k,int s,int l,int pop,int q,int pivot);
void display(int a[],int k);
int main()
{
    int k,s,l,pop,q,pivot;
    printf("How many elements do you want to enter\n");
    k=input();
    int a[k];
    printf("Enter the elements of the array\n");
    input_array(a,k);
    for(int i=0;i<k;i++)
        printf("%d ",a[i]);
    printf("\n");
    compute(a,k,s,l,pop,q,pivot);
    display(a,k);    
}
int input()
{
    int k;
    scanf("%d",&k);
    return k;
}
void input_array(int a[],int k)
{
    for(int i=0;i<k;i++)
    scanf("%d",&a[i]);
}
void compute(int a[],int k,int s,int l,int pop,int q,int pivot)
{
    s=0,l=0,pop=k-1,q=0,pivot=a[0];
    for(int i=1;i<k;i++)
    {
        if(a[i]<=a[0])
            s++;// s is number of elements smaller than the pivot
        else
        {
            l++;// l is the number of elements larger than the pivot
            pop--;// pop is the position or index of pivot after partition
        }
    }
    printf("s=%d l=%d pop= %d\n",s,l,pop);
    for(int i=1;i<k;i++)
    {
        if(i!=pop)
        {
            if(a[i]<pivot)
            {
                a[q]=a[i];
                q++;
            }
            else if(a[i]>pivot)
            {
                for(int z=k;z>pop;z--)
                {
                    if(a[z]<pivot)
                    {
                        int b=a[i];
                        a[i-1]=a[z];
                        a[z]=b;
                    }
                }
            }
        }
        else
        {
            for(int z=k;z>=pop;z--)
                {
                    if(a[z]<pivot)
                    {
                        int b=a[i];
                        a[i-1]=a[z];
                        a[z]=b;
                    }
                }
            a[i]=pivot;
        }
    }
}
void display(int a[],int k)
{
    for(int i=0;i<k;i++)
        printf("%d ",a[i]);
    printf("\n");
}